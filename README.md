JSON Pipes
=====

Customizable JSON to JSON transforming tool, inspired by Angular.

Tests
-----

```sh
$ npm test
```

Features
--------

* _Simple familiar syntax_

  Syntax is based on Angular template pipes syntax, but more strict and, hence, declarative.
  
* _Still JSON_

  JSON with pipes has no problem with JSON validity. Just indicate string that it should be processed with help of ` symbol.

* _Extensible_

  You are welcome to add new pipes with any amount of parameters.

* _Short API_

  Tool has good and ease to use API. Just 3 methods to chain together: `use` pipes, `compile` template and `execute` it over data.

* _Asyncronious_

  Template execution is completely asyncronious. If pipes you are using are well designed, you will get even more asyncronisity. Read further for more.

API
---

> ```ts
> use(alias: string, pipe: Pipe): this
> ```

Use this method when you wish to extend compiler with custom pipes. For example, in order to use pipe `makegood` you have to register it within the jsonpipes instance, using this method. Note, that this method return `this` instance and you are able to chain methods and write declarative compact code.

> ```ts
> compile(template: any): this
> ```

This method takes any validly parsed JSON as input and compile it into specially optimized structure, that can be executed rapidly over provided data. You can cache instance of JSONPipes after this mathod invoked in order to execute it later. Note, that this method return `this` instance and you are able to chain methods and write declarative compact code.

> ```ts
> execute(done: NodeCallback, data?: any): this
> ```

Use this method only after some template is compiled via `.compile`. This method will execute all jsonpipes occurrences in compiled template, using optinally provided data. As soon as execution is competed, `done` callback will be executed with error (if any) as first argument and result (if no errors) as second argument. Note, that this method return `this` instance and you are able to chain methods and write declarative compact code.

Examples
--------

When creating a JSONPipes instance, you are allowed to provide a few options in order to customize execution behavior. Currently, there are two of them: `entries` and `eliminate`.

Assign a list of strings to `eliminate` option in order to specify which fields of execution result should be eliminated. For example, if you have a `definitions` field, that you will only address in your JSON, then you may want to eliminate them in result. For example:

_template.json:_
```json
{
  "eliminate": {},
  "keep": {
    "eliminate": {}
  }
}
```

```ts
import { jsonpipes } from '@bracketedrebels/jsonpipes';
import * as template from './template.json';

jsonpipes({ eliminate: ['eliminate', 'keep.eliminate'] })
  .compile(template)
  .execute(compiled => console.log(compiled));
  // outputs { keep: {} }
```

Assign a list of strings to `entries` option in order to specify what fields values should be considered as executable. All fields of provided template that are not in provided list will not be executed. For example:

_data.json:_
```json
{ 
  "answer": {
    "truthy": 42,
    "falsy": 13
  }
}
```

_template.json:_
```json
{
  "executed" : "`answer.truthy`",
  "ignored" : "`answer.falsy`"
}
```

```ts
import { jsonpipes } from '@bracketedrebels/jsonpipes';
import * as data from './data.json';
import * as template from './template.json';

jsonpipes({ entries: ['executed'] })
  .compile(template)
  .execute(compiled => console.log(compiled), data);
  // outputs { executed: 42, ignored: '`answer.falsy`' }
```

<!-- Also you should know, that there are handful pipes already exists under [special npm namespace](https://www.npmjs.com/org/jsonpipes). -->
