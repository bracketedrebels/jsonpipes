import 'mocha';
import { expect } from 'chai';
import * as parser from './parser';

const parse = parser.parse.bind(parser);

// tslint:disable
describe('Grammar parser', () => {
  it('should correctly parse empty string', () => {
    expect(parse(``)).to.be.empty;
  })

  it('should correctly parse simple string tokens', () => {
    expect(parse(`''`)).to.deep.include('')
    expect(parse(`'string'`)).to.deep.include('string')
  })

  it('should correctly parse simple number tokens', () => {
    expect(parse(`1`)).to.deep.include(1)
    expect(parse(`-1`)).to.deep.include(-1)
    expect(parse(`-1e2`)).to.deep.include(-1e2)
    expect(parse(`1e2`)).to.deep.include(1e2)
    expect(parse(`1.25`)).to.deep.include(1.25)
    expect(parse(`-1.25`)).to.deep.include(-1.25)
    expect(parse(`.1e2`)).to.deep.include(.1e2)
    expect(parse(`-.1e2`)).to.deep.include(-.1e2)
    expect(parse(`1.42e10`)).to.deep.include(1.42e10)
    expect(parse(`-1.42e10`)).to.deep.include(-1.42e10)
    expect(parse(`1.42e-10`)).to.deep.include(1.42e-10)
    expect(parse(`-1.42e-10`)).to.deep.include(-1.42e-10)
  })

  it('should correctly parse simple boolean tokens', () => {
    expect(parse(`true`)).to.deep.include(true)
    expect(parse(`false`)).to.deep.include(false)
  })

  it('should correctly parse query sugar tokens', () => {
    expect(parse(`a`)).to.be.deep.equal(['a', {get: []}])
    expect(parse(`a.b.c`)).to.be.deep.equal(['a.b.c', {get: []}])
  })

  it('should correctly parse no arguments pipe', () => {
    expect(parse(`1 | pipe`)).to.be.deep.equal([1, {pipe: []}])
    expect(parse(`'string' | pipe`)).to.be.deep.equal(['string', {pipe: []}])
    expect(parse(`a.b.c | pipe`)).to.be.deep.equal(['a.b.c', {get: []}, {pipe: []}])
  })

  it('should correctly parse pipe with simple arguments', () => {
    expect(parse(`1 | pipe : 1`)).to.be.deep.equal([1, {pipe: [1]}])
    expect(parse(`1 | pipe : 1 : 2`)).to.be.deep.equal([1, {pipe: [1, 2]}])
    expect(parse(`1 | pipe : 1 : 2 : 3`)).to.be.deep.equal([1, {pipe: [1, 2, 3]}])
  })

  it('should correctly parse pipe with query argument', () => {
    expect(parse(`1 | pipe : a.b.c`)).to.be.deep.equal([1, {pipe: [['a.b.c', {get: []}]]}])
  })

  it('should correctly parse pipe chain', () => {
    expect(parse(`1 | pipe | pipe`)).to.be.deep.equal([1, {pipe: []}, {pipe: []}])
    expect(parse(`1 | pipe | pipe | pipe`)).to.be.deep.equal([1, {pipe: []}, {pipe: []}, {pipe: []}])
    expect(parse(`1 | pipe : 2 | pipe`)).to.be.deep.equal([1, {pipe: [2]}, {pipe: []}])
    expect(parse(`1 | pipe : 2 | pipe : 3`)).to.be.deep.equal([1, {pipe: [2]}, {pipe: [3]}])
  })

  it('should correctly parse pipes in pipe param', () => {
    expect(parse(`1 | pipe : (2 | pipe | pipe) | pipe`)).to.be.deep.equal([1, {pipe: [[2, {pipe: []}, {pipe: []}]]}, {pipe: []}])
    expect(parse(`1 | pipe : (2 | pipe : (3 | pipe))`)).to.be.deep.equal([1, {pipe: [[2, {pipe: [[3, {pipe: []}]]}]]}])
    expect(parse(`1 | pipe : 2 : (3 | pipe)`)).to.be.deep.equal([1, {pipe: [2, [3, {pipe: []}]]}])
  })

  it('should not allow to use parentheses for wrapping simple pipe arguments', () => {
    expect(() => parse(`1 | pipe : (1)`)).to.throw()
    expect(() => parse(`1 | pipe : 1 : (a.b.c)`)).to.throw()
  })

  it('should not allow to use parentheses for execution order changing', () => {
    expect(() => parse(`(1)`)).to.throw()
    expect(() => parse(`1 | pipe | (pipe | pipe)`)).to.throw()
  })

  it('should not allow to use invalid tokens', () => {
    expect(() => parse(`1e`)).to.throw()
    expect(() => parse(`1 | 1nvalid`)).to.throw()
  })

  it('should not allow to use different count of opening and closing parentheses', () => {
    expect(() => parse(`1 | pipe : ((2 | pipe)`)).to.throw()
    expect(() => parse(`1 | pipe : (2 | pipe))`)).to.throw()
  })
})
