export interface Options {
  entries?: string[];
  eliminate?: string[];
}

export interface Context {
  readonly options: Options;
  readonly template?: any;
  readonly data?: any;
}

export interface NoParamsPipe {
  (data: any, done: NodeCallback, context: Context): void;
}

export interface Pipe extends NoParamsPipe {
  (data: any, done: NodeCallback, context: Context, ...params: any[]): void;
}

export interface NodeCallback { (err: any, success?: any): void }
export interface SuccessCallback { (success?: any): void }
export interface Dict<T> { [k: string]: T }
export interface Environment {
  atom: number | string | boolean | ExecutionAtom | undefined,
  stack: Expression,
  done: NodeCallback,
  pipes: Dict<Pipe>,
  context: Context,
  input?: any,
  params: any[] }
export interface Expression extends Array<number | string | boolean | ExecutionAtom> {}
export interface ExecutionAtom {
  [index: string]: Array<number | string | boolean | Expression>;
}

export interface TemplateEntry {
  path: string[];
  expr: Expression;
}
