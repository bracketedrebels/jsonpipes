import { NodeCallback, Context } from './interfaces';


export function get(input: string, done: NodeCallback, context: Context): void {
  done(null, input
    .split('.')
    .reduce( (acc, v) => acc ? acc[v] : acc, context.data ));
}
