import 'mocha';
import { expect } from 'chai';
import { execute } from './execute';
import { get } from './get';
import { optionsDefault } from './consts';
import { Context } from './interfaces';

describe('Expression Executor', () => {
  it('should execute empty expression',
    validate(v => expect(v).to.be.empty))
  it('should execute numbers',
    validate(v => expect(v).to.be.equal(42), [42]))
  it('should execute false',
    validate(v => expect(v).to.be.false, [false]))
  it('should execute true',
    validate(v => expect(v).to.be.true, [true]))
  it('should execute strings',
    validate(v => expect(v).to.be.equal('string'), ['string']))

  it('should execute correct queries over satisfying data',
    validate(v => expect(v).to.be.equal(1),
      ['a.b', {get: []}], 
      { get }, 
      {options: optionsDefault, data: {a: {b: 1}}}))
  
  it('should execute queries over data that not satisfied',
    validate(v => expect(v).to.be.equal(undefined),
      ['a.b', {get: []}], 
      { get }, 
      {options: optionsDefault, data: {}}))

  it('should execute two-layered expressions correctly',
    validate(v => expect(v).to.be.equal(9),
      [ 2, { add: [[ 3, { add: [4] } ]] } ], 
      { add: (arg, next, _, ...adders) => next(null, adders.reduce( (acc, v) => acc + v, 0) + arg) }));
  
  it('should execute multi-layered expressions correctly',
    validate(v => expect(v).to.be.equal(109),
      [ 2, { add: [[ 3, { add: [[ 4, { mul: [[ 5, { add: [6, 7, 8] } ]] } ]] } ]] } ], 
      {
        add: (arg, next, _, ...adders) => next(null, adders.reduce( (acc, v) => acc + v, 0) + arg),
        mul: (arg, next, _, ...mults) => next(null, mults.reduce( (acc, v) => acc * v, 1) * arg)
      }));
  
})

function validate(successor, input = [], pipes = {}, context: Context = { options: optionsDefault }) {
  return done => execute(input, pipes, context, (err, v) =>
    err ? done(err) : (successor(v), done()));
}
