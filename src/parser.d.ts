import { Expression } from './interfaces';

declare function parse(v: string): Expression;
