import { Options } from './interfaces';
import { JSONPipes } from './jsonpipes';

export { Context } from './interfaces';
export function jsonpipes(options: Options) {
  return new JSONPipes(options);
}
