import { Context, Dict, NodeCallback, Environment, Expression, Pipe, SuccessCallback } from './interfaces';
import * as run from 'run-parallel';

export function execute(expr: Expression, pipes: Dict<Pipe>, context: Context, done: NodeCallback): void {
  if (expr.length > 0) {
    chain({ atom: expr.shift(), context, done, params: [], pipes, stack: expr  });
  } else {
    done(null, '');
  }
}

function chain(env: Environment): void {
  const doner = (success: SuccessCallback) => (err: any, result: any) => err ? env.done(err) : success(result);
  if (typeof env.atom === 'object') {
    const pipeName = Object.keys(env.atom)[0];
    const pipeParams = (env.atom[pipeName] || []) as any[];
    const subexpressions = pipeParams.filter( v => Array.isArray(v));
    const pipeDoner = doner(result => env.stack.length > 0
      ? chain({...env, input: result, atom: env.stack.shift()})
      : env.done(null, result));
    const continueExecution = (params: any[]) => env.pipes[pipeName](env.input, pipeDoner, env.context, ...params);
    run(
      subexpressions
        .map(expr => (cb: NodeCallback) => execute(expr, env.pipes, env.context, cb)),
      doner(results => continueExecution(pipeParams
          .map(param => Array.isArray(param)
            ? results[subexpressions.indexOf(param)]
            : param ))));
  } else {
    const atom = env.atom;
    if (env.stack.length > 0) {
      chain({...env, input: atom, atom: env.stack.shift()});
    } else {
      env.done(null, atom);
    }
  }
}
