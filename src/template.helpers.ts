import { TemplateEntry, NodeCallback, Dict, Pipe, Context } from './interfaces';
import { execute } from './execute';

export function shouldBeExecuted(entries: string[]): (v: TemplateEntry) => boolean {
  return ({path}: TemplateEntry) =>
    entries.length
      ? entries.indexOf(path.join('.')) >= 0
      : true;
}

export function runnerFactory(pipes: Dict<Pipe>, context: Context): (v: TemplateEntry) => (cb: NodeCallback) => void {
  return ({path, expr}: TemplateEntry) =>
    (cb: NodeCallback) =>
      execute(expr, pipes, context, runnerCallbackFactory(path, cb));
}

export function completer(template: any, eliminate: string[], result: any): any {
  return eliminate
    .reduce( (tmpl, v) => { 
      const path = v.split('.');
      const parent = path.slice(0, -1).reduce((acc, step) => acc && acc[step], tmpl);
      const child = path.pop();
      if (parent && child && typeof parent === 'object' && child in parent) {
        delete parent[child];
      }
      return tmpl;
    }, result.reduce(replaceExpressionWithResult, JSON.parse(JSON.stringify(template))));
} 


function runnerCallbackFactory(path: string[], cb: NodeCallback): NodeCallback {
  return (err, result) =>
    err
      ? cb(err)
      : cb(null, {result, path});
}

function replaceExpressionWithResult(tmpl: any, v: {path: string[], result: any}): any {
  const parent = v.path
    .slice(0, -1)
    .reduce((acc, step) => acc[step], tmpl);
  parent[v.path[v.path.length - 1]] = v.result;
  return tmpl;
}
