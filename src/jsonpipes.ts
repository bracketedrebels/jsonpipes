import { Pipe, Options, NodeCallback } from './interfaces';
import { optionsDefault } from './consts';
import { Template } from './template';
import { get } from './get';


export class JSONPipes {
  public use(alias: string, pipe: Pipe): this {
    this.pipes[alias] = pipe;
    return this;
  }

  public compile(template: any): this {
    this.templateCompiled = new Template(template, this.pipes);
    return this;
  }

  public execute(done: NodeCallback, data?: any): this {
    this.templateCompiled.execute(data, this.options, done);
    return this;
  }

  public set eliminate(v: string[]) {
    this.options = { ...this.options, eliminate: v };
  }

  public set entries(v: string[]) {
    this.options = { ...this.options, entries: v };
  }

  constructor( options?: Options ) {
    this.options = { ...optionsDefault, ...options };
  }


  private options: Options = optionsDefault;
  private pipes: {[k: string]: Pipe} = {'get': get};
  private templateCompiled: Template;
}
