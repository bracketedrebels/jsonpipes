import 'mocha';
import { expect } from 'chai';
import { Template } from './template';
import { get } from './get';
import { optionsDefault } from './consts';

// tslint:disable

const context = {
  options: optionsDefault,
  data: null
}

describe('Template', () => {
  describe('with default options and no data', () => {
    before(() => context.options = optionsDefault);
    it('should be executed correctly while being boolean', validate(v =>
      expect(v).to.be.true, true));
    it('should be executed correctly while being null', validate(v =>
      expect(v).to.be.null, null));
    it('should be executed correctly while being number', validate(v =>
      expect(v).to.be.equal(42), 42));
    it('should be executed correctly while being string', validate(v =>
      expect(v).to.be.equal('string'), 'string'));
    it('should be executed correctly while being template without expressions', validate(v =>
      expect(v).to.be.deep.equal({a: 'test'}), {a: 'test'}));
    it('should be executed correctly while being template with single expression', validate(v =>
      expect(v).to.be.deep.equal({a: false}), {a: '`false`'}));
    it('should be executed correctly while being template with multiple expressions', validate(v =>
      expect(v).to.be.deep.equal({a: false, b: true, c: 42, d: 'str'}),
      {a: '`false`', b: '`true`', c: '`42`', d: "`'str'`"}));
  })

  describe('with custom entries and no data', () => {
    before(() => context.options = { ...optionsDefault, entries: ['a', 'b.c'] });
    it('should be executed correctly while being template without expressions', validate(v =>
      expect(v).to.be.deep.equal({a: 'test'}), {a: 'test'}));
    it('should be executed correctly while being template with single expression which is entry', validate(v =>
      expect(v).to.be.deep.equal({a: false}), {a: '`false`'}));
    it('should be executed correctly while being template with single expression which is not entry', validate(v =>
      expect(v).to.be.deep.equal({b: '`false`'}), {b: '`false`'}));
    it('should be executed correctly while being template with multiple expressions which are all entries',
      validate(v =>
        expect(v).to.be.deep.equal({a: false, b: { c: 42 }}), {a: '`false`', b: { c: '`42`' }}));
  })

  describe('with elimination and no data', () => {
    before(() => context.options = { ...optionsDefault, eliminate: ['a', 'b.c'] });
    it('should be correctly executed while being simple and eliminated', validate(v =>
      expect(v).to.be.equal(42), 42));
    it('should be correctly executed and fully eliminated', validate(v =>
      expect(v).to.be.deep.equal({}), {a: '`false`'}));
    it('should be correctly executed and partly eliminated', validate(v =>
      expect(v).to.be.deep.equal({b: 2}), {a: 4, b: 2}));
    it('should be correctly executed and partly eliminated', validate(v =>
      expect(v).to.be.deep.equal({b: {}}), {a: '`false`', b: { c: '`42`' }}));
  })
})


function validate(cb, template): (done: (e?: any) => void) => any {
  return done => (new Template(template, { get }))
    .execute(context.data, context.options, (err, result) => err
      ? done(err)
      : (cb(result), done()));
}
