import { Dict, Pipe, NodeCallback, TemplateEntry, Options } from './interfaces';
import { compile } from './compile';
import * as run from 'run-parallel';
import { shouldBeExecuted, runnerFactory, completer } from './template.helpers';

export class Template {
  public execute(data: any, options: Options, done: NodeCallback): any {
    const runners = this.compiled
      .filter( shouldBeExecuted(options.entries || []) )
      .map( runnerFactory(this.pipes, { data, options, template: this.source }) );

    run( runners, (err: any, result: {result: any, path: string[]}[]) => err
      ? done(err)
      : done(null, completer(this.source, options.eliminate || [], result)))
  }

  constructor(template: any, pipes?: Dict<Pipe>) {
    this.source = template;
    this.pipes = pipes || {};
    this.compiled = compile(template, pipes);
  }


  private source: any;
  private pipes: Dict<Pipe>;
  private compiled: TemplateEntry[];
}
