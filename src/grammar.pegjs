tokens  = t:token ps:pipe*
          { return [...t, ...ps] }
        / ''
          { return [] }
argcomplex = colon '(' _ t:token ps:pipe+ _ ')'
          { return [[...t, ...ps]] }
argsimple = colon t:token
      { return t }
pipe	= vline name:identifier params:(argsimple / argcomplex)*
          { return { [name]: params.map(v => v.length > 1 ? v : v[0]) } }
token   = number / string / boolean / query
query   = value:$(identifier (dot (letter / digit)+)*)
          { return [
            value,
            { get: [] }
          ] }
string  = "'" value:$([^']*) "'"
          { return [value] }
        / '"' value:$([^"]*) '"'
          { return [value] }
boolean = 'true'
          { return [true] }
        / 'false'
          { return [false] }
number  = v:$(minus? digits? dot? digits 'e' minus? digits)
          { return [parseFloat(v)] }
        / v:$(minus? digits? dot digits)
          { return [parseFloat(v)] }
        / v:$(minus? digits 'e' minus? digits)
          { return [parseFloat(v)] }
        / v:$(minus? digits)
          { return [parseInt(v)] }

vline = $(_ '|' _)
colon  = $(_ ':' _)
digits = $digit+
identifier = $(letter (letter / digit)*)
digit = [0-9]
letter = [a-zA-Z]
dot = '.'
minus = '-'
_  = [ \t\r\n]*