import { Options, Context } from './interfaces';

export const optionsDefault: Options = {
  eliminate: [],
  entries: []
};

export const context: Context = {
  data: null,
  options: {},
  template: null
}
