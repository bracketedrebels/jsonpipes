import { Dict, Pipe, TemplateEntry } from './interfaces';
import * as parser from './parser';
import * as traverse from 'traverse';

export function compile(template: any, pipes: Dict<Pipe> = {}): TemplateEntry[] {
  return traverse(template)
    .reduce(function(acc, v) {
      if (this.isLeaf && isPipes(v)) {
        const content: string = (v as string).substr(1, v.length - 2)
        const expr = parser.parse(content);
        traverse(expr).forEach( ({name}) => name && validatePipeDeclaration(name, pipes));
        return [...acc, { path: this.path, expr }];
      }
      return acc;
    }, []);
}

export function validatePipeDeclaration(name: string, pipes: Dict<Pipe>): void {
  if (!(name in pipes)) {
    throw new Error(`Undeclared pipe '${name}' detected.`);
  }
}

export function isPipes(v: any) {
  return typeof v === 'string' && !!v.match(/`[\s\S]*?`/);
}
