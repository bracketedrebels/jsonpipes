import 'mocha';
import { expect } from 'chai';
import { compile, isPipes, validatePipeDeclaration } from './compile';

// tslint:disable

describe('Template compiler', () => {
  it('should correctly detect pipeful leaves of JSON', () => {
    expect(isPipes(null)).to.be.false
    expect(isPipes(true)).to.be.false
    expect(isPipes(42)).to.be.false
    expect(isPipes([])).to.be.false
    expect(isPipes({})).to.be.false
    expect(isPipes('')).to.be.false
    expect(isPipes('``')).to.be.true
    expect(isPipes('`asdf`')).to.be.true
  })

  it('should not validate undeclared pipes', () => {
    expect(() => validatePipeDeclaration('pipe', {})).to.throw
    expect(() => validatePipeDeclaration('pipe', {'pipe': () => {}})).not.to.throw
  })

  it('should correctly process json without pipes', () => {
    expect(compile({ a: 'string' })).to.be.empty
  })

  it('should correctly process json with pipes', () => {
    expect(compile({ a: '``' })).to.deep.members([{ path: ['a'], expr: []}])
    expect(compile({ a: '``' })).to.deep.members([{ path: ['a'], expr: []}])
    expect(compile({ a: { b: { c: '``' }}})).to.deep.members([{ path: ['a', 'b', 'c'], expr: []}])
    expect(compile({ a: '``', b: '``', c: '``'})).to.deep.members([{ path: ['a'], expr: []}, { path: ['b'], expr: []}, { path: ['c'], expr: []}])
  })

  it('should not pass pipes that are not declared', () => {
    expect(() => compile({ a: '`a`' })).to.throw
  })

  it('should pass pipes that are fully declared', () => {
    expect(() => compile({ a: '`a`' }, { get: () => {}})).not.to.throw
  })
})
